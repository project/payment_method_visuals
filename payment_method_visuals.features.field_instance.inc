<?php
/**
 * @file
 * payment_method_visuals.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function payment_method_visuals_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'payment_method-payment_method-field_visuals'
  $field_instances['payment_method-payment_method-payment_method_visuals'] = array(
    'bundle' => 'payment_method',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'payment_method',
    'field_name' => 'payment_method_visuals',
    'label' => 'Visuals',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'entity_translation_sync' => FALSE,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Visuals');

  return $field_instances;
}
