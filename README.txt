Payment Method Visuals

Overview
--------
The Payment Method Visuals replace the payment method selection widget used on
payment form by radio elements which can use visuals as labels for the payment
methods. The visuals used as label for a payment method can be configured when
editing the payment method. Visuals are managed through a field on the Payment
Method entity type through the Fieldable Payment Method module.

TODO
----
 - Remove features dependency and manage add field in a hook_install()
   implementation.
 - Provides a way for a Payment Method's controller to provide default value for
   the payment_method_visuals field.